const express = require("express");
const mongoose = require("mongoose");
const app = express();

const User = require("./routes/user");
const Login = require('./routes/login');
const Guest = require('./routes/guest');
const cors = require('./middlewares/cors');

app.use(express.json());
app.use(cors);
app.use('/api/Guests', Guest);
app.use('/api/Users', User);
app.use('/api/Login', Login);

mongoose.connect('mongodb://localhost/test_guest_house')
    .then(() => console.log("GuestHouse connected"))
    .catch(err => console.error("there is error"));

const port = process.env.PORT || 4000;
const server = app.listen(port, () => {
    console.log(`server connected on port ${port}`);
});

module.exports = server;