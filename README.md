This is the backend of the project that is built with Node.js, express.js and
mongodb. It also includes test files that contain around 20 independent test
cases.

Setup
Make sure to follow all these steps exactly as explained below. Do not miss any 
steps or you won't be able to run this application.

Install MongoDB
To run this project, you need to install the latest version of MongoDB Community
Edition first.

https://docs.mongodb.com/manual/installation/

Once you install MongoDB, make sure it's running.

Install the Dependencies

Next, from the project folder, install the dependencies:

npm i

Populate the Database. 
mongoimport --db test_guest_house --collection users --file users.json --jsonArray

Run the Tests
You're almost done! Run the tests to make sure everything is working:

The email can be found in the database.
The password for the manager and reception is 12345678

npm test

All tests should pass.

Start the Server
nodemon index.js
This will launch the Node server on port 4000. 