const request = require('supertest');
const {User} = require('../../../models/users');

let server;
describe('/api/Login', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach( () => { 
      server.close(); 
    });
    describe('POST /', () => {

        let token;  
        let email, password;

        const exec =  () => {
        return  request(server)
            .post('/api/Login')
            .set('x-auth-token', token)
            .send({ 
                email : 'test@gmail.com',
                password: '12345678', 
            });
        }

        beforeEach(async () => {
        token = new User().generateAuthToken(); 
        let users = new User({name:"testing", email : 'teste@gmail.com',
        password: '12345678'});

        users = await users.save();
        });

        it('should return 400 if the user has an invalid input', async () => {
        
        
        const res = await request(server)
        .post('/api/Login')
        .set('x-auth-token', token)
        .send({ 
            email : 'test@gmail.com',
            password: '1234', 
        });

        expect(res.status).toBe(400);
        });

        it('should return 400 if the user has an unexisting email', async () => {
        email = 'testo@gmail.com'; 
        
        const res = await request(server)
        .post('/api/Login')
        .set('x-auth-token', token)
        .send({ 
            email : 'testo@gmail.com',
            password: '12345678', 
        });

        expect(res.status).toBe(400);
        });

        it('should return 400 if the user has an incorrect password', async () => {
        // password = '11111111'; 
        
        const res = await request(server)
        .post('/api/Login')
        .set('x-auth-token', token)
        .send({ 
            email : 'testo@gmail.com',
            password: '11111111', 
        });

        expect(res.status).toBe(400);
        });


        it('should return a token if the user is valid', async () => {
        token = new User({name : "testing", email:"test@gmail.com",password : "12345678", isManager : false}).generateAuthToken();
        const res =await request(server)
        .post('/api/Login')
        .set('x-auth-token', token)
        .send({ 
            email : 'teste@gmail.com',
            password: '12345678', 
        });

        expect(res.status).toBe(200);
        });
        
    });

});