const {User} = require('../../../models/users');
const {Guest} = require('../../../models/guests');
const request = require('supertest');

let server;
let token;
let guest; 

describe('auth middleware', () => {
  beforeEach(() => {
     server = require('../../../index'); 
     token = new User({name : "testing", email:"testing@gmail.com",password : "12345", isManager : true}).generateAuthToken();
     guest = { 
        name: 'guest1',
        woreda : 1,
        city: 'city1',
        subcity: 'subcity',
        house_no: 1234,
        phone_no: '1234567890123',
        Id_no: 'Id_no1',
        purpose: 'purpose1' 
    };
    });
  afterEach(async () => { 
    await Guest.remove({});
    server.close(); 
  });

  

  const exec = () => {
    return request(server)
      .post('/api/guests')
      .set('x-auth-token', token)
      .send(guest);
  }

  it('should return 401 if no token is provided', async () => {
    token = ''; 

    const res = await exec();

    expect(res.status).toBe(401);
  });

  it('should return 400 if token is invalid', async () => {
    token = 'a'; 

    const res = await exec();

    expect(res.status).toBe(400);
  });

  it('should return 200 if token is valid', async () => {
    const res = await exec();

    expect(res.status).toBe(200);
  });
});