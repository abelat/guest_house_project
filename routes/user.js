const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {
    User,
    validate
} = require('../models/users');

router.post("/", async (req, res) => {
    const {
        error
    } = validate(req.body);

    if (error) return res.status(400).send(error.details[0].message);

    const salt = await bcrypt.genSalt(10);
  const hashed = await bcrypt.hash(req.body.password, salt);

    let user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashed
    });

    user = await user.save();
    return res.send(user);
});

module.exports = router;